import {Link} from 'react-router-dom';

export default function SignIn() {
  return (
    <div className="p3 max-w-lg mx-auto">
    <h1 className="text-xl-3 text-center font-semibold my-7">Login</h1>
    <form className="gap-4 flex flex-col ">

      <input type="text" placeholder="E-mail" id="email" className="p-3 border rounded-lg"/>
      <input type="password" placeholder="Password" id="password" className="p-3 border rounded-lg"/>
      <button className="bg-slate-900 text-white p-3 rounded-lg uppercase hover:opacity-95 disabled:opacity-80">Sign In</button>
    </form>

    <div className='flex gap-2 mt-5'>
      <p>Have an account?</p>
      <Link to={'/sign-up'}>
        <span className='text-blue-700 font-semibold'>Sign up</span>
      </Link>
    </div>
</div>
  )
}
