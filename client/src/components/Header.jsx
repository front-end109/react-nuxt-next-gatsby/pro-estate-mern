import { Link  } from 'react-router-dom';
import { FaSearch } from 'react-icons/fa';


export default function Header() {
  return (
    <header className="bg-slate-900 shadow-md ">
      <div className="flex justify-between items-center max-w-6xl mx-auto p-3">
        
        <Link to='/'> 
          <h1 className="font-bold text-sm sm:text-xl flex flex-wrap">
              <span className="text-slate-500">Pro</span>  
              <span className="text-cyan-50">Estate</span>
          </h1>
        </Link>

        <form className="bg-slate-200 p3 rounded-lg flex items-center ">
          <input type="text" placeholder="Search..." className='bg-transparent focus:outline-none  pl-2 w-24 sm:w-64' />
          <button>
            <FaSearch className='text-slate-600 pr-1' />
          </button>
        </form>

        <ul className='text-white flex gap-4'>
          <Link to='/'>
            <li className='hidden sm:inline text-cyan-50 hover:underline'>
              Home
            </li>
          </Link>
          <Link to='/about'>
            <li className='hidden sm:inline text-cyan-50 hover:underline'>
              About
            </li>
          </Link >

          <Link to='/sign-in'>
            <li className='text-cyan-50 hover:underline'>Sign-in</li>
          </Link>
        
        </ul>
      </div>

    </header>
  )
}
