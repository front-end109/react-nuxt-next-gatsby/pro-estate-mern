import User from "../models/user.model.js";
import bcrypt from 'bcrypt';
import { errorHandler } from "../utils/error.js";

export const signup = async (req,res, next) => {

    // destructure req
    const { username, email, password } = req.body;
    
    const salt = bcrypt.genSaltSync(10);
    const password_hash = bcrypt.hashSync(password, salt);
    
    const newUser = new User({username, email, password: password_hash});
    try {
        await newUser.save();
        res.status(201).json('User created successfully!');

    } catch (error) {
        next(error);
    }
}


