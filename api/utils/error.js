export const errorHandler = (statusCode, message) => {
    const _error = new Error();

    _error.statusCode  = statusCode;
    _error.message = message;

    return _error;
};