import express from "express";
// const express = require('express')
import mongoose from "mongoose";
import dotenv from 'dotenv';
import userRouter from './routes/user.route.js';
import authRouter from './routes/auth.route.js';

dotenv.config();
const app = express();
app.use(express.json())

const PORT = process.env.PORT || 3000;

// mongoDB connection
mongoose.connect(process.env.MONGO_CONNECT)
.then(() => {
    console.log("Connected to mongoDB!");
})
.catch((err)=> {
    console.error("Something bad has happened with mongoDB., ", err)
});

app.listen(PORT , ()=> {
    console.info(`listening on port, ${PORT}`);
});

// routes
app.use('/api/user', userRouter);
app.use('/api/auth', authRouter);

// middleware
app.use((err, req, res, next) => {
    const statusCode = err.statusCode || 500;
    const message = err.message || 'Internal Server Error.';

    return res.status(statusCode).json({
        success: false,
        statusCode: statusCode,
        message});
});